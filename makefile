install: Main.hs BrowserInterface.hs EntryCursor.hs
	ghc Main.hs -o sbrowser -threaded
	strip sbrowser
	rm *.hi && rm *.o